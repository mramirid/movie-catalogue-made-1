package com.mramirid.moviecatalogue.activity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.movie.Movie;
import com.mramirid.moviecatalogue.movie.MovieAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TypedArray dataPhoto;
    private String[] dataName, dataGenres, dataDuration, dataDescription;
    private int[] dataYear;
    private MovieAdapter adapter;
    private ArrayList<Movie> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new MovieAdapter(this);
        ListView listView = findViewById(R.id.tv_list);
        listView.setAdapter(adapter);

        prepare();
        addItem();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent moveWithObjectIntent = new Intent(MainActivity.this, MovieDescriptionActivity.class);
                moveWithObjectIntent.putExtra(MovieDescriptionActivity.KEY_EXTRA, movies.get(i));
                startActivity(moveWithObjectIntent);
            }
        });
    }

    private void prepare() {
        dataPhoto = getResources().obtainTypedArray(R.array.data_photo);
        dataName = getResources().getStringArray(R.array.data_name);
        dataGenres = getResources().getStringArray(R.array.data_genres);
        dataYear = getResources().getIntArray(R.array.data_year);
        dataDuration = getResources().getStringArray(R.array.data_duration);
        dataDescription = getResources().getStringArray(R.array.data_description);
    }

    private void addItem() {
        movies = new ArrayList<>();

        for (int i = 0; i < dataName.length; ++i) {
            Movie movie = new Movie();
            movie.setPhoto(dataPhoto.getResourceId(i, -1));
            movie.setName(dataName[i]);
            movie.setGenres(dataGenres[i]);
            movie.setYear(dataYear[i]);
            movie.setDuration(dataDuration[i]);
            movie.setDescription(dataDescription[i]);
            movies.add(movie);
        }
        adapter.setMovies(movies);
    }
}
