package com.mramirid.moviecatalogue.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.mramirid.moviecatalogue.R;
import com.mramirid.moviecatalogue.movie.Movie;

import java.util.Objects;

public class MovieDescriptionActivity extends AppCompatActivity {

    public static final String KEY_EXTRA = "key_extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_description);

        ImageView imgCoverDetail = findViewById(R.id.img_cover_detail);
        ImageView imgPhotoDetail = findViewById(R.id.img_photo_detail);
        TextView txtNameDetail = findViewById(R.id.txt_name_detail);
        TextView txtGenresDetail = findViewById(R.id.txt_genres_detail);
        TextView txtYearDetail = findViewById(R.id.txt_year_detail);
        TextView txtDurationDetail = findViewById(R.id.txt_duration_detail);
        TextView txtDescriptionDetail = findViewById(R.id.txt_description_detail);

        Movie movie = getIntent().getParcelableExtra(KEY_EXTRA);
        assert movie != null;
        Glide.with(this).load(movie.getPhoto()).centerCrop().into(imgCoverDetail);
        Glide.with(this).load(movie.getPhoto()).into(imgPhotoDetail);
        txtNameDetail.setText(movie.getName());
        txtGenresDetail.setText(movie.getGenres());
        txtYearDetail.setText(String.valueOf(movie.getYear()));
        txtDurationDetail.setText(movie.getDuration());
        txtDescriptionDetail.setText(movie.getDescription());

        Objects.requireNonNull(getSupportActionBar()).setTitle(movie.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
