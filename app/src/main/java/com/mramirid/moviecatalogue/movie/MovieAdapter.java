package com.mramirid.moviecatalogue.movie;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mramirid.moviecatalogue.R;

import java.util.ArrayList;

public class MovieAdapter extends BaseAdapter {

    private final Context context;
    private ArrayList<Movie> movies;

    public MovieAdapter(Context context) {
        this.context = context;
        movies = new ArrayList<>();
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int i) {
        return movies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null)
            view = LayoutInflater.from(context).inflate(R.layout.item_movie, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);
        Movie movie = (Movie) getItem(i);
        viewHolder.bind(movie);
        return view;
    }

    private class ViewHolder {
        private TextView txtName, txtGenres, txtYear, txtDuration;
        private ImageView imgPhoto;

        ViewHolder(View view) {
            imgPhoto = view.findViewById(R.id.img_photo);
            txtName = view.findViewById(R.id.txt_name);
            txtGenres = view.findViewById(R.id.txt_genres);
            txtYear = view.findViewById(R.id.txt_year);
            txtDuration = view.findViewById(R.id.txt_duration);
        }

        void bind(Movie movie) {
            Glide.with(context).load(movie.getPhoto()).into(imgPhoto);
            txtName.setText(movie.getName());
            txtGenres.setText(movie.getGenres());
            txtYear.setText(String.valueOf(movie.getYear()));
            txtDuration.setText(movie.getDuration());
        }
    }
}
